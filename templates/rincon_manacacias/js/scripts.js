(function(window, $){         
    function scrollTo(target) {
        //var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    $(document).ready(function() {
        $(window).resize(function() {                
            if ($("#top-menu-resp").is(":visible")){
                $("#header-top ul:first-child").hide();
            }
            else{                    
                $("#header-top ul:first-child").show();
            }
        });

        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul:first-child").toggle("slow");
        });
        //$('img[usemap]').rwdImageMaps();
        $("#lnk_top").click(function(e){
            e.preventDefault();
            scrollTo("#top-link");
        });
        
        $("#btnContacto").click(function(e) {
            e.preventDefault();
            $("#loading").addClass("loading");
            $.ajax({
                url: $("#form_cita").attr("action"),
                data: $("#form_cita").serialize(),
                method: 'post',
                success: function(res){
                    $("#loading").removeClass("loading");
                    $("#form_cita #nombre").val("");
                    $("#form_cita #email").val("");
                    $("#form_cita #comentarios").val("");
                    alert(res);
                }
            });
        });
        /*$("#banner_home_1, #banner_home_2").lightSlider({
            item: 1,
            pager: false,
            enableDrag: true,
            controls: true,
            loop: true
        });
        
        $("#lnkBanner1").click(function(e){
            e.preventDefault();
            $("#banner_home_1").toggle("slow");
            if ($(this).hasClass("boton_arriba")){
                $(this).removeClass("boton_arriba").addClass("boton_abajo");
            }
            else{
                $(this).removeClass("boton_abajo").addClass("boton_arriba");
            }
        });
        
        $("#header-top ul.nav.menu li.divider").each(function(i, o) {
            $(this).empty();
            $(this).addClass("logo-menu").append('<img src="/templates/aliarse/img/logo.png" />');
        });
        
        //$("#my_slider_2 a").featherlight();*/
        $(document).foundation();
	});
})(window, jQuery);