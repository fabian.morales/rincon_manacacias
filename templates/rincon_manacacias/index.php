<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
@ini_set('display_errors', '1');
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets
JHtml::_('jquery.framework', true, true);
//$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/css/foundation.min.css');
$doc->addScript('templates/'.$this->template.'/foundation/js/foundation.min.js');
$doc->addScript('templates/'.$this->template.'/foundation/js/vendor/modernizr.js');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');
$doc->addScript('templates/'.$this->template.'/js/scripts.js');
// Add current user information
$user = JFactory::getUser();
$menu = $app->getMenu();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body>
    <div id="top-link"></div>
    <header <?php if ($menu->getActive() == $menu->getDefault()) : ?>class="home"<?php endif; ?>>
        <div class="contenido">
            <div class="row">
                <div class="small-12 medium-3 columns">
                    <a href="index.php"><img src="templates/rincon_manacacias/img/logo.png" /></a>
                </div>
                <div class="small-12 medium-9 columns text-right">
                    <div class="row" id="header-top">
                        <div class="small-12 columns">
                            <div id="top-menu-resp" class="menu-resp">
                                <div>Menu</div>
                                <a href="#"></a>
                            </div>
                        </div>
                        <div class="small-12 columns">
                            <jdoc:include type="modules" name="top-menu" style="xhtml" />
                        </div>
                    </div>
                </div>            
            </div>
            <?php if($this->countModules('in-banner')) : ?>
            <div class="separador">&nbsp;</div>
            <div class="row info collapse">
                <div class="small-12 columns">
                    <jdoc:include type="modules" name="in-banner" style="xhtml" />
                </div>
            </div>
            <?php endif; ?>
        </div>
    </header>
    
    <?php if($this->countModules('sub-banner')) : ?>
    <div class="row seccion">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="sub-banner" style="xhtml" />
        </div>
    </div>
    <?php endif; ?>
    
    <?php if($this->countModules('franja-amarilla')) : ?>
    <div class="franja_amarilla">
        <div class="row">
            <div class="small-12 columns">
                <jdoc:include type="modules" name="franja-amarilla" style="xhtml" />
            </div>
        </div>
    </div>
    <?php endif; ?>
    
    <?php
        if ($menu->getActive() != $menu->getDefault()) : ?>
        <div class="component">
            <div class="row">
                <div class="small-12 columns">
                    <jdoc:include type="component" />
                </div>
            </div>
        </div>        
    <?php endif; ?>
    
    <footer>        
        <div class="contacto">
            <div class="row">
                <div class="small-12 columns text-center">
                    <jdoc:include type="modules" name="footer-contacto" style="xhtml" />
                </div>
            </div>
            <?php if($this->countModules('footer')) : ?>
            <div class="row">
                <div class="small-12 medium-10 columns medium-centered">
                    <div class="franja_gris">
                        <div class="row">
                            <div class="small-12 columns">
                                <jdoc:include type="modules" name="footer" style="xhtml" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="row fullWidth copy">
            <div class="small-12 columns text-center">
                <jdoc:include type="modules" name="footer-redes" style="xhtml" />
            </div>
            <div class="small-12 columns text-center">
                <div>Todos los derechos reservados. 2015</div>                
            </div>
        </div>
    </footer>
</body>
</html>
